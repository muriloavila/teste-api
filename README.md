# Teste - API

### Instalação

### Docker

```sh
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
```

### docker-compose
```sh
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```
### No Diretorio do Projeto

```sh
docker-compose build
docker-compose up -d
```
### Import Database

```sh
docker exec api_db mysql -u root -proot -e "CREATE DATABASE api;"
docker exec -it api_db bash
mysql -u root -proot api < /home/api.sql
exit
```
### Dependencias do Projeto (Composer e limpeza de Cache)

```sh
docker-compose exec web composer update -vvv
docker-compose exec web sh limpeza.sh
```
### Projeto Disponível em:

```sh
http://localhost:8100
```