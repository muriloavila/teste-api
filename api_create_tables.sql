USE api

create table company (
  	CP_ID int auto_increment not null primary key,
	CP_NAME VARCHAR(500) NOT NULL,
	CP_CATCHPHRASE TEXT NOT NULL,
	CP_BS TEXT NOT NULL
);

create table users (
  	US_ID int auto_increment not null primary key,
	US_NAME VARCHAR(500) NOT NULL,
	US_USERNAME VARCHAR(500) NOT NULL,
	US_EMAIL VARCHAR(500) NOT NULL,
	US_PHONE VARCHAR(500) NOT NULL,
	US_WEBSITE VARCHAR(500) NOT NULL,
	US_CP_ID INT NOT NULL,
	
	foreign key (US_CP_ID) references company(CP_ID)
);

CREATE TABLE address
(
	ADDR_ID INT auto_increment not null primary key,
	ADDR_US_ID INT NOT NULL,
	ADDR_STREET VARCHAR(500) NOT NULL, 
	ADDR_SUITE VARCHAR(500) NULL,
	ADDR_CITY VARCHAR(500) NOT NULL,
	ADDR_ZIPCODE VARCHAR(500) NOT NULL,
	
	foreign key (ADDR_US_ID) references users(US_ID)
);

CREATE TABLE geolocation
(
	GEO_ID INT auto_increment not null primary key,
	GEO_ADDR_ID INT,
	GEO_LAT VARCHAR(500) NOT NULL,
	GEO_LNG VARCHAR(500) NOT NULL,
	
	foreign key (GEO_ADDR_ID) references address(ADDR_ID)
);


CREATE TABLE post
(
	PS_ID INT auto_increment not null primary key,
	PS_US_ID INT,
	PS_TITLE TEXT NOT NULL,
	PS_BODY TEXT,
	
	foreign key (PS_US_ID) references users(US_ID)
);
