#!bin/bash
echo "iniciando limpeza"

chmod 777 * -R

vendor/sensio/distribution-bundle/Sensio/Bundle/DistributionBundle/Resources/bin/build_bootstrap.php
php app/console cache:clear --env=prod
php app/console cache:clear --env=dev
php app/console cache:warmup --env=prod
php app/console cache:warmup --env=dev

php app/console doctrine:cache:clear-metadata
php app/console doctrine:cache:clear-result
php app/console doctrine:cache:clear-query

chmod 777 * -R

echo 'flush_all' | nc localhost 11211

echo "Limpeza concluida"

