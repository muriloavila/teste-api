function buscaDados(id){
    $.ajax({
        method: "GET",
        url: "/app.php/users/"+id,
      }).done(function(data){
        $.each(data, function(index,value){
            $("#"+index).val(value);

            if(index == 'address'){
                $("#AddrStreet").val(value.addrStreet);
                $("#AddrSuite").val(value.addrSuite);
                $("#AddrCity").val(value.addrCity);
                $("#AddrZip").val(value.addrZipcode);

                $("#GeoLat").val(value.geo.geoLat);
                $("#GeoLng").val(value.geo.geoLng)
            }

            if(index == 'usCp'){
                $("#cpName").val(value.cpName);
                $("#cpCatchPhrase").val(value.cpCatchphrase);
                $("#cpBs").val(value.cpBs);
            }
        });
      });
}

function atualizaDados(){
    var id = $("#id").val();

    var dados = [{
            "name": $("#usName").val(),
            "username": $("#usUsername").val(),
            "email": $("#usEmail").val(),
            "address": {
                "street":   $("#AddrStreet").val(),
                "suite":    $("#AddrSuite").val(),
                "city":     $("#AddrCity").val(),
                "zipcode":  $("#AddrZip").val(),
                "geo": {
                        "lat":$("#GeoLat").val(),
                        "lng": $("#GeoLng").val()
                    }
                },
            "phone": $("#usPhone").val(),
            "website": $("#usWebsite").val(),
            "company": {
                "name":         $("#cpName").val(),
                "catchPhrase":  $("#cpCatchPhrase").val(),
                "bs":           $("#cpBs").val()
            }
    }];

    $.ajax({
        method: "PUT",
        url:    "/app.php/users/"+id,
        data: JSON.stringify(dados)
    }).done(function(dados){
        alert(dados.resposta);
    });
}


function insereDados(){
        var id = $("#id").val();
    
        var dados = [{
                "name": $("#usName").val(),
                "username": $("#usUsername").val(),
                "email": $("#usEmail").val(),
                "address": {
                    "street":   $("#AddrStreet").val(),
                    "suite":    $("#AddrSuite").val(),
                    "city":     $("#AddrCity").val(),
                    "zipcode":  $("#AddrZip").val(),
                    "geo": {
                            "lat":$("#GeoLat").val(),
                            "lng": $("#GeoLng").val()
                        }
                    },
                "phone": $("#usPhone").val(),
                "website": $("#usWebsite").val(),
                "company": {
                    "name":         $("#cpName").val(),
                    "catchPhrase":  $("#cpCatchPhrase").val(),
                    "bs":           $("#cpBs").val()
                }
        }];
        console.log(dados);
        $.ajax({
            method: "POST",
            url:    "/app.php/users/",
            data: JSON.stringify(dados)
        }).done(function(dados){
            alert(dados.resposta);
        });
}

$(document).ready(function(){
    if($("#id").val() != undefined && $("#id").val() > 0){
        buscaDados($("#id").val());
    }

    $("#singlebutton").click(function(){
        if($("#id").val() != undefined && $("#id").val() > 0){
            atualizaDados();
        }else{
            insereDados();
        }
        
        return false;
    });
});