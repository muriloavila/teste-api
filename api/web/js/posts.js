'<div class="card">'+
'  <div class="card-header">'+
'    Quote'+
'  </div>'+
'  <div class="card-body">'+
'        <blockquote class="blockquote mb-0">'+
'        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>'+
'        <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>'+
'        </blockquote>'+
'    </div>'+
'</div>';


function carregaPosts(id){
    $.ajax({
        method: "GET",
        url: "/app.php/users/"+id+"/posts"
    }).done(function(data){
        var html = "";
        $.each(data, function(index, value){
            html += '<div class="card">'+
                    '  <div class="card-body">'+
                    '        <blockquote class="blockquote mb-0">'+
                    '        <p>'+value.psTitle+'</p>'+
                    '        <footer class="blockquote-footer">'+value.psBody+
                    '        </footer>'+
                    '        </blockquote>'+
                    '    </div>'+
                    '</div>';            
        });

        $("#content").append(html);
    });
}



$(document).ready(function(){
    carregaPosts($("#id").val());
});