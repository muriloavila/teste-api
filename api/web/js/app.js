function buscaUsuarios(){
  $.ajax({
    method: "GET",
    url: "/app.php/users",
  }).done(function(data){
    var td = "";
    $.each(data, function(index, value){
      td += "<tr>"+
            " <td>"+value.usName+"</td>"+
            " <td>"+value.usUsername+"</td>"+
            " <td>"+value.usEmail+"</td>"+
            " <td>"+value.address.addrStreet+"</td>"+
            " <td>"+value.address.addrSuite+"</td>"+
            " <td>"+value.address.addrCity+"</td>"+
            " <td>"+value.address.addrZipcode+"</td>"+
            " <td>"+value.address.geo.geoLat+"</td>"+
            " <td>"+value.address.geo.geoLng+"</td>"+
            " <td>"+value.usPhone+"</td>"+
            " <td>"+value.usWebsite+"</td>"+
            " <td>"+value.usCp.cpName+"</td>"+
            " <td>"+value.usCp.cpCatchphrase+"</td>"+
            " <td>"+value.usCp.cpBs+"</td>"+
            ' <td><a href="/app.php/view/user/'+value.usId+'" class="btn btn-info btn-sm">Alter</a></td>'+
            ' <td><input type="button" onclick=deleteUser("'+value.usId+'") class="btn btn-danger btn-sm" value="Delete"></input>'+"</td>"+
            ' <td><a href="/app.php/view/posts/'+value.usId+'" class="btn btn-primary btn-sm">Show Posts</a>'+"</td>"+
            "</tr>";
    });

    $("#user-table").append(td);

    $('#dtUsuarios').DataTable({
      "pagingType": "full_numbers",
      "lengthChange": false,
      "searching": false, // "simple" option for 'Previous' and 'Next' buttons only
      "ordering": false
    });
    $('.dataTables_length').addClass('bs-select');
  });
}

function deleteUser(id){
  if(!confirm('Deseja Realizar a Exclusão do Usuário?')){
    return false;
  }

  $.ajax({
    method: "DELETE",
    url: "/app.php/users/"+id
  }).done(function(data){
    alert(data.resposta);

    location.reload();
  });
}

$(document).ready(function () {
  buscaUsuarios();
});