<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Acl\Exception\Exception;

class CompanyRepository extends EntityRepository {
    public function findOneByName($name){
        return $this->getEntityManager()->getRepository('AppBundle:Company')->findOneBy(['cpName' => $name]);
    }
}
?>