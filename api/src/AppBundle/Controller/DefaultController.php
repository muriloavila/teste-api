<?php

namespace AppBundle\Controller;

use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // BUSCA OS usuarios
        
        return $this->render('default/index.html.twig');
        
    }

    public function viewPostsAction(Request $request, $id){
        return $this->render('default/posts.html.twig', array('id'=>$id));
    }

    public function alterUserAction(Request $request, $id){
        return $this->render('default/user.html.twig', array('id'=>$id));
    }

    public function newUserAction(Request $request){
        return $this->render('default/user.html.twig', array('id'=>0));
    }

}
