<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\Geolocation;
use AppBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;
use Exception;

class UsersController extends Controller
{
    public function showAllAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:Users')->findAll();
        $retorno = array();
        foreach($users as $key => $user){
            $retorno[$key] = $user->toArray();
            $address = $em->getRepository('AppBundle:Address')->findOneBy(['addrUs' => $user]);

            $retorno[$key]['address'] = $address->toArray();
            $geo = $em->getRepository('AppBundle:Geolocation')->findOneBy(['geoAddr' => $address]);

            $retorno[$key]['address']['geo'] = $geo->toArray();
        }

        return new JsonResponse($retorno);
    }

    public function showUserAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:Users')->findOneBy(['usId' => $id]);
        $retorno = array();

        $retorno = $user->toArray();
        $address = $em->getRepository('AppBundle:Address')->findOneBy(['addrUs' => $user]);

        $retorno['address'] = $address->toArray();
        $geo = $em->getRepository('AppBundle:Geolocation')->findOneBy(['geoAddr' => $address]);

        $retorno['address']['geo'] = $geo->toArray();

        return new JsonResponse($retorno);
    }

    public function insertAction(Request $request){
        try{
            $data = json_decode($request->request->get('data'), true);
            if(empty($data)){
                $data = json_decode($request->getContent(), true);
            }
            $em = $this->getDoctrine()->getManager();
            
            foreach($data as $user){
                
                try{
                    
                    $company = new Company();
                    $company->setCpName($user['company']['name']);
                    $company->setCpCatchphrase($user['company']['catchPhrase']);
                    $company->setCpBs($user['company']['bs']);

                    $em->persist($company);
                    

                    $nuser = new Users();
                    $nuser->setUsName($user['name']);
                    $nuser->setUsUsername($user['username']);
                    $nuser->setUsEmail($user['email']);
                    $nuser->setUsPhone($user['phone']);
                    $nuser->setUsWebsite($user['website']);
                    $nuser->setUsCp($company);

                    $em->persist($nuser);

                    $address = new Address();
                    $address->setAddrStreet($user['address']['street']);
                    $address->setAddrSuite($user['address']['suite']);
                    $address->setAddrCity($user['address']['city']);
                    $address->setAddrZipcode($user['address']['zipcode']);
                    $address->setAddrUs($nuser);
                    
                    $em->persist($address);

                    $geo = new Geolocation();
                    $geo->setGeoAddr($address);
                    $geo->setGeoLat($user['address']['geo']['lat']);
                    $geo->setGeoLng($user['address']['geo']['lng']);

                    $em->persist($geo);

                    $em->flush();

                }catch(Exception $e){
                    
                    return new JsonResponse(["resposta" => 'Erro: ' . $e->getMessage()]);
                }
            }
        }catch(Exception $e){
            return new JsonResponse(["resposta" => $e->getMessage()]);    
        }
        

        return new JsonResponse(["resposta" => 'Dados Inseridos com sucesso']);
    }

    public function updateAction(Request $request, $id){
        try{
            $em = $this->getDoctrine()->getManager();
            $data = json_decode($request->getContent(), true);

            $user = $em->getRepository('AppBundle:Users')->findOneBy(['usId' => $id]);

            $company = $em->getRepository('AppBundle:Company')->findOneBy(['cpId' => $user->getUsCp()->getCpId()]);

            $address = $em->getRepository('AppBundle:Address')->findOneBy(['addrUs' => $user]);

            $geolocation = $em->getRepository('AppBundle:Geolocation')->findOneBy(['geoAddr' => $address]);

            if(empty($user)){
                return new JsonResponse(["resposta" => 'Erro! Usuário Nao encontrado']);
            }

            $company->setCpName($data[0]['company']['name']);
            $company->setCpCatchphrase($data[0]['company']['catchPhrase']);
            $company->setCpBs($data[0]['company']['bs']);

            $em->persist($company);

            $user->setUsName($data[0]['name']);
            $user->setUsUsername($data[0]['username']);
            $user->setUsEmail($data[0]['email']);
            $user->setUsPhone($data[0]['phone']);
            $user->setUsWebsite($data[0]['website']);

            $em->persist($user);

            $address->setAddrStreet($data[0]['address']['street']);
            $address->setAddrSuite($data[0]['address']['suite']);
            $address->setAddrCity($data[0]['address']['city']);
            $address->setAddrZipcode($data[0]['address']['zipcode']);

            $em->persist($address);

            $geolocation->setGeoLat($data[0]['address']['geo']['lat']);
            $geolocation->setGeoLng($data[0]['address']['geo']['lng']);

            $em->persist($geolocation);

            $em->flush();

        }catch(Exception $e){
            return new JsonResponse(["resposta" => 'Erro!'.$e->getMessage()]);
        }
        return new JsonResponse(["resposta" => 'Altera os dados trazido via Json do usuario: '.$id]);
    }

    public function deleteAction(Request $request, $id){
        try{
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:Users')->findOneBy(['usId' => $id]);
            if(empty($user)){
                return new JsonResponse(["resposta" => 'Erro! Usuário Nao encontrado']);
            }

            $posts = $em->getRepository('AppBundle:Post')->findBy(['psUs' => $id]);
            $company = $em->getRepository('AppBundle:Company')->findOneBy(['cpId' => $user->getUsCp()->getCpId()]);
            $address = $em->getRepository('AppBundle:Address')->findOneBy(['addrUs' => $user]);
            $geolocation = $em->getRepository('AppBundle:Geolocation')->findOneBy(['geoAddr' => $address]);

            if(!empty($posts)){
                foreach($posts as $post){
                    $em->remove($post);
                }
            }

            $em->remove($geolocation);
            $em->remove($address);
            $em->remove($user);
            $em->remove($company);

            $em->flush();
        }catch(Exception $e){
            return new JsonResponse(["resposta" => 'Erro!'.$e->getMessage()]);
        }
        return new JsonResponse(["resposta" => 'Deletado o Registro do Usuario: '.$id]);
    }
}
