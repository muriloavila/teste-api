<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PostsController extends Controller
{
    public function showPostsAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:Users')->findOneBy(['usId' => $id]);

        $retorno = array();
        try{
            $posts = $em->getRepository('AppBundle:Post')->findBy(['psUs' => $id]);

            foreach($posts as $post){
                $retorno[] = $post->toArray();
            }
        }catch(Exception $e){
            $retorno = ['message' => $e->getMessage()];
        }
        
        return new JsonResponse($retorno);
    }

    public function insertAction(Request $request){
        $data = json_decode($request->request->get('data'), true);
        $em = $this->getDoctrine()->getManager();

        try{
            foreach($data as $key => $post){
                $user = $em->getRepository('AppBundle:Users')->findOneBy(['usId' => $post['userId']]);
    
                $npost = new Post();
                $npost->setPsUs($user);
                $npost->setPsTitle($post['title']);
                $npost->setPsBody($post['body']);
    
                $em->persist($npost);
            }

            $em->flush();
    
        }catch(Exception $e){
            return new JsonResponse(["resposta" => 'Erro!'.$e->getMessage()]);    
        }
        
        return new JsonResponse(["resposta" => 'registros inseridos com sucesso']);
    }

    public function updateAction(Request $request, $id){
        try{
            $em = $this->getDoctrine()->getManager();

            $post = $em->getRepository('AppBundle:Post')->findOneBy(['psId' => $id]);

            $data = json_decode($request->getContent(), true);

            if(empty($post)){
                return new JsonResponse(["resposta" => 'Erro! Post Nao encontrado']);
            }

            $post->setPsTitle($data[0]['title']);
            $post->setPsBody($data[0]['body']);

            $em->persist($post);

            $em->flush();
        }catch(Exception $e){
            return new JsonResponse(["resposta" => 'Erro!'.$e->getMessage()]);
        }
        return new JsonResponse(["resposta" => 'alteração realizada com sucesso']);
    }

    public function deleteAction(Request $request, $id){
        try{
            $em = $this->getDoctrine()->getManager();

            $post = $em->getRepository('AppBundle:Post')->findOneBy(['psId' => $id]);

            if(empty($post)){
                return new JsonResponse(["resposta" => 'Erro! Post Nao encontrado']);
            }
            
            $em->remove($post);
            $em->flush();
        }catch(Exception $e){
            return new JsonResponse(["resposta" => 'Erro!'.$e->getMessage()]);
        }
        return new JsonResponse(["resposta" => 'Post Excluído com Sucesso']);
    }
}
