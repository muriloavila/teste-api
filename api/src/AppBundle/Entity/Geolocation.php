<?php

namespace AppBundle\Entity;

/**
 * Geolocation
 */
class Geolocation
{
    /**
     * @var integer
     */
    private $geoId;

    /**
     * @var string
     */
    private $geoLat;

    /**
     * @var string
     */
    private $geoLng;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $geoAddr;


    /**
     * Get geoId
     *
     * @return integer
     */
    public function getGeoId()
    {
        return $this->geoId;
    }

    /**
     * Set geoLat
     *
     * @param string $geoLat
     *
     * @return Geolocation
     */
    public function setGeoLat($geoLat)
    {
        $this->geoLat = $geoLat;

        return $this;
    }

    /**
     * Get geoLat
     *
     * @return string
     */
    public function getGeoLat()
    {
        return $this->geoLat;
    }

    /**
     * Set geoLng
     *
     * @param string $geoLng
     *
     * @return Geolocation
     */
    public function setGeoLng($geoLng)
    {
        $this->geoLng = $geoLng;

        return $this;
    }

    /**
     * Get geoLng
     *
     * @return string
     */
    public function getGeoLng()
    {
        return $this->geoLng;
    }

    /**
     * Set geoAddr
     *
     * @param \AppBundle\Entity\Address $geoAddr
     *
     * @return Geolocation
     */
    public function setGeoAddr(\AppBundle\Entity\Address $geoAddr = null)
    {
        $this->geoAddr = $geoAddr;

        return $this;
    }

    /**
     * Get geoAddr
     *
     * @return \AppBundle\Entity\Address
     */
    public function getGeoAddr()
    {
        return $this->geoAddr;
    }

    public function toArray(){
        return [
            'geoId' => $this->getGeoId(),
            'geoLat' => $this->getGeoLat(),
            'geoLng' => $this->getGeoLng()
        ];
    }
}

