<?php

namespace AppBundle\Entity;

/**
 * Users
 */
class Users
{
    /**
     * @var integer
     */
    private $usId;

    /**
     * @var string
     */
    private $usName;

    /**
     * @var string
     */
    private $usUsername;

    /**
     * @var string
     */
    private $usEmail;

    /**
     * @var string
     */
    private $usPhone;

    /**
     * @var string
     */
    private $usWebsite;

    /**
     * @var \AppBundle\Entity\Company
     */
    private $usCp;


    /**
     * Get usId
     *
     * @return integer
     */
    public function getUsId()
    {
        return $this->usId;
    }

    /**
     * Set usName
     *
     * @param string $usName
     *
     * @return Users
     */
    public function setUsName($usName)
    {
        $this->usName = $usName;

        return $this;
    }

    /**
     * Get usName
     *
     * @return string
     */
    public function getUsName()
    {
        return $this->usName;
    }

    /**
     * Set usUsername
     *
     * @param string $usUsername
     *
     * @return Users
     */
    public function setUsUsername($usUsername)
    {
        $this->usUsername = $usUsername;

        return $this;
    }

    /**
     * Get usUsername
     *
     * @return string
     */
    public function getUsUsername()
    {
        return $this->usUsername;
    }

    /**
     * Set usEmail
     *
     * @param string $usEmail
     *
     * @return Users
     */
    public function setUsEmail($usEmail)
    {
        $this->usEmail = $usEmail;

        return $this;
    }

    /**
     * Get usEmail
     *
     * @return string
     */
    public function getUsEmail()
    {
        return $this->usEmail;
    }

    /**
     * Set usPhone
     *
     * @param string $usPhone
     *
     * @return Users
     */
    public function setUsPhone($usPhone)
    {
        $this->usPhone = $usPhone;

        return $this;
    }

    /**
     * Get usPhone
     *
     * @return string
     */
    public function getUsPhone()
    {
        return $this->usPhone;
    }

    /**
     * Set usWebsite
     *
     * @param string $usWebsite
     *
     * @return Users
     */
    public function setUsWebsite($usWebsite)
    {
        $this->usWebsite = $usWebsite;

        return $this;
    }

    /**
     * Get usWebsite
     *
     * @return string
     */
    public function getUsWebsite()
    {
        return $this->usWebsite;
    }

    /**
     * Set usCp
     *
     * @param \AppBundle\Entity\Company $usCp
     *
     * @return Users
     */
    public function setUsCp(\AppBundle\Entity\Company $usCp = null)
    {
        $this->usCp = $usCp;

        return $this;
    }

    /**
     * Get usCp
     *
     * @return \AppBundle\Entity\Company
     */
    public function getUsCp()
    {
        return $this->usCp;
    }

    public function toArray(){
        return [
            'usId' => $this->usId,
            'usName' => $this->usName,
            'usUsername' => $this->usUsername,
            'usEmail' => $this->usEmail,
            'usPhone' => $this->usPhone,
            'usWebsite' => $this->usWebsite,
            'usCp' => $this->usCp->toArray()
        ];
    }
}

