<?php

namespace AppBundle\Entity;

/**
 * Company
 */
class Company
{
    /**
     * @var integer
     */
    private $cpId;

    /**
     * @var string
     */
    private $cpName;

    /**
     * @var string
     */
    private $cpCatchphrase;

    /**
     * @var string
     */
    private $cpBs;


    /**
     * Get cpId
     *
     * @return integer
     */
    public function getCpId()
    {
        return $this->cpId;
    }

    /**
     * Set cpName
     *
     * @param string $cpName
     *
     * @return Company
     */
    public function setCpName($cpName)
    {
        $this->cpName = $cpName;

        return $this;
    }

    /**
     * Get cpName
     *
     * @return string
     */
    public function getCpName()
    {
        return $this->cpName;
    }

    /**
     * Set cpCatchphrase
     *
     * @param string $cpCatchphrase
     *
     * @return Company
     */
    public function setCpCatchphrase($cpCatchphrase)
    {
        $this->cpCatchphrase = $cpCatchphrase;

        return $this;
    }

    /**
     * Get cpCatchphrase
     *
     * @return string
     */
    public function getCpCatchphrase()
    {
        return $this->cpCatchphrase;
    }

    /**
     * Set cpBs
     *
     * @param string $cpBs
     *
     * @return Company
     */
    public function setCpBs($cpBs)
    {
        $this->cpBs = $cpBs;

        return $this;
    }

    /**
     * Get cpBs
     *
     * @return string
     */
    public function getCpBs()
    {
        return $this->cpBs;
    }

    public function toArray(){
        return [
            'cpId' => $this->getCpId(),
            'cpName' => $this->getCpName(),
            'cpCatchphrase' => $this->getCpCatchphrase(),
            'cpBs' => $this->getCpBs()
        ];
    }
}

