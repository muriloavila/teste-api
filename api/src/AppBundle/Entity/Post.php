<?php

namespace AppBundle\Entity;

/**
 * Post
 */
class Post
{
    /**
     * @var integer
     */
    private $psId;

    /**
     * @var string
     */
    private $psTitle;

    /**
     * @var string
     */
    private $psBody;

    /**
     * @var \AppBundle\Entity\Users
     */
    private $psUs;


    /**
     * Get psId
     *
     * @return integer
     */
    public function getPsId()
    {
        return $this->psId;
    }

    /**
     * Set psTitle
     *
     * @param string $psTitle
     *
     * @return Post
     */
    public function setPsTitle($psTitle)
    {
        $this->psTitle = $psTitle;

        return $this;
    }

    /**
     * Get psTitle
     *
     * @return string
     */
    public function getPsTitle()
    {
        return $this->psTitle;
    }

    /**
     * Set psBody
     *
     * @param string $psBody
     *
     * @return Post
     */
    public function setPsBody($psBody)
    {
        $this->psBody = $psBody;

        return $this;
    }

    /**
     * Get psBody
     *
     * @return string
     */
    public function getPsBody()
    {
        return $this->psBody;
    }

    /**
     * Set psUs
     *
     * @param \AppBundle\Entity\Users $psUs
     *
     * @return Post
     */
    public function setPsUs(\AppBundle\Entity\Users $psUs = null)
    {
        $this->psUs = $psUs;

        return $this;
    }

    /**
     * Get psUs
     *
     * @return \AppBundle\Entity\Users
     */
    public function getPsUs()
    {
        return $this->psUs;
    }

    public function toArray(){
        return [
            'psId' => $this->getPsId(),
            'psTitle' => $this->getPsTitle(),
            'psBody' => $this->getPsBody()
        ];
    }
}

