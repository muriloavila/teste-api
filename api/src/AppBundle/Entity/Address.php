<?php

namespace AppBundle\Entity;

/**
 * Address
 */
class Address
{
    /**
     * @var integer
     */
    private $addrId;

    /**
     * @var string
     */
    private $addrStreet;

    /**
     * @var string
     */
    private $addrSuite;

    /**
     * @var string
     */
    private $addrCity;

    /**
     * @var string
     */
    private $addrZipcode;

    /**
     * @var \AppBundle\Entity\Users
     */
    private $addrUs;


    /**
     * Get addrId
     *
     * @return integer
     */
    public function getAddrId()
    {
        return $this->addrId;
    }

    /**
     * Set addrStreet
     *
     * @param string $addrStreet
     *
     * @return Address
     */
    public function setAddrStreet($addrStreet)
    {
        $this->addrStreet = $addrStreet;

        return $this;
    }

    /**
     * Get addrStreet
     *
     * @return string
     */
    public function getAddrStreet()
    {
        return $this->addrStreet;
    }

    /**
     * Set addrSuite
     *
     * @param string $addrSuite
     *
     * @return Address
     */
    public function setAddrSuite($addrSuite)
    {
        $this->addrSuite = $addrSuite;

        return $this;
    }

    /**
     * Get addrSuite
     *
     * @return string
     */
    public function getAddrSuite()
    {
        return $this->addrSuite;
    }

    /**
     * Set addrCity
     *
     * @param string $addrCity
     *
     * @return Address
     */
    public function setAddrCity($addrCity)
    {
        $this->addrCity = $addrCity;

        return $this;
    }

    /**
     * Get addrCity
     *
     * @return string
     */
    public function getAddrCity()
    {
        return $this->addrCity;
    }

    /**
     * Set addrZipcode
     *
     * @param string $addrZipcode
     *
     * @return Address
     */
    public function setAddrZipcode($addrZipcode)
    {
        $this->addrZipcode = $addrZipcode;

        return $this;
    }

    /**
     * Get addrZipcode
     *
     * @return string
     */
    public function getAddrZipcode()
    {
        return $this->addrZipcode;
    }

    /**
     * Set addrUs
     *
     * @param \AppBundle\Entity\Users $addrUs
     *
     * @return Address
     */
    public function setAddrUs(\AppBundle\Entity\Users $addrUs = null)
    {
        $this->addrUs = $addrUs;

        return $this;
    }

    /**
     * Get addrUs
     *
     * @return \AppBundle\Entity\Users
     */
    public function getAddrUs()
    {
        return $this->addrUs;
    }

    public function toArray(){
        return [
            'addrId' => $this->getAddrId(),
            'addrStreet' => $this->getAddrStreet(),
            'addrSuite' => $this->getAddrSuite(),
            'addrCity' => $this->getAddrCity(),
            'addrZipcode' => $this->getAddrZipcode()
        ];
    }
}

